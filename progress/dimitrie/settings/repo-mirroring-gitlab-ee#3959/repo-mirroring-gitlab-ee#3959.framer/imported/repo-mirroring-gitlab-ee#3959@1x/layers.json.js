window.__imported__ = window.__imported__ || {};
window.__imported__["repo-mirroring-gitlab-ee#3959@1x/layers.json.js"] = [
	{
		"objectId": "2C75086A-682C-4C23-B759-3AF9FE51650A",
		"kind": "artboard",
		"name": "view1",
		"originalName": "view1",
		"maskFrame": null,
		"layerFrame": {
			"x": -1065,
			"y": -2720,
			"width": 1500,
			"height": 2275
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "C7215699-0441-4D78-910D-D635664073D2",
				"kind": "group",
				"name": "reference",
				"originalName": "reference",
				"maskFrame": null,
				"layerFrame": {
					"x": 231,
					"y": 188,
					"width": 1069,
					"height": 1988
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-reference-qzcymtu2.jpg",
					"frame": {
						"x": 231,
						"y": 188,
						"width": 1069,
						"height": 1988
					}
				},
				"children": []
			},
			{
				"objectId": "65A04D7B-A4A1-4F2E-A188-67F3BE612F1E",
				"kind": "group",
				"name": "breadcrumbs",
				"originalName": "breadcrumbs",
				"maskFrame": null,
				"layerFrame": {
					"x": 255,
					"y": 16,
					"width": 990,
					"height": 32
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-breadcrumbs-njvbmdre.png",
					"frame": {
						"x": 255,
						"y": 16,
						"width": 990,
						"height": 32
					}
				},
				"children": [
					{
						"objectId": "608E1E74-3C8C-47A7-A528-418E5A17A304",
						"kind": "group",
						"name": "Group",
						"originalName": "Group",
						"maskFrame": null,
						"layerFrame": {
							"x": 255,
							"y": 16,
							"width": 400,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group-nja4rtff.png",
							"frame": {
								"x": 255,
								"y": 16,
								"width": 400,
								"height": 16
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "237ABD1A-67A6-41B0-897B-D9ABF8E72652",
				"kind": "group",
				"name": "section_accordion_collapsed",
				"originalName": "section-accordion--collapsed",
				"maskFrame": null,
				"layerFrame": {
					"x": 255,
					"y": 80,
					"width": 990,
					"height": 65
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-section_accordion_collapsed-mjm3quje.png",
					"frame": {
						"x": 255,
						"y": 80,
						"width": 990,
						"height": 65
					}
				},
				"children": [
					{
						"objectId": "1C836141-A02D-45E5-90B6-E2FE064817F3",
						"kind": "group",
						"name": "expand_button",
						"originalName": "expand-button",
						"maskFrame": null,
						"layerFrame": {
							"x": 1173,
							"y": 80,
							"width": 72,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-expand_button-mum4mzyx.png",
							"frame": {
								"x": 1173,
								"y": 80,
								"width": 72,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "DBBA592C-F918-4DFD-BDA7-0D5CE5FBBB32",
						"kind": "group",
						"name": "collapse_button",
						"originalName": "collapse-button",
						"maskFrame": null,
						"layerFrame": {
							"x": 1166,
							"y": 80,
							"width": 79,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-collapse_button-rejcqtu5.png",
							"frame": {
								"x": 1166,
								"y": 80,
								"width": 79,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "F76FA511-5D9D-4674-A41E-258B47F9AC9B",
						"kind": "group",
						"name": "header",
						"originalName": "header",
						"maskFrame": null,
						"layerFrame": {
							"x": 255,
							"y": 85,
							"width": 846,
							"height": 60
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-header-rjc2rke1.png",
							"frame": {
								"x": 255,
								"y": 85,
								"width": 846,
								"height": 60
							}
						},
						"children": []
					}
				]
			}
		]
	}
]